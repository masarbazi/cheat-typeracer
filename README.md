# Cheat TypeRacer

Cheat paly.typeracer.com :D

How it works?! This is no magic, it's just DOM querying and manipulation :D

# How to use
Sorry I couldn't publish this on chrome web store but anyway this should be simple... \
Go ahead and download project as a zip file and extract. \
In chrome go to your extensions tab and turn on Developer mode on the top right corner then click Load unpacked.\
Select project root folder. It should be cheat-typeracer-master\
You are all set. Now let's CHEAT :D

There are two modes of using this extension: 
1. Auto
2. Manual 

**Manual**: Join a new race and hit the Cheat button on extension popup. After race begins press Ctrl + / for words to get typed.\
Here is a preview of using extension: `https://d.pr/free/v/lfqNZD`

**Auto**: On this mode words get typed time by time (The time that you provid on extension popup input (in ms)). After race begin hit the Cheat button. Just **Notice**: on auto mode, hover mouse over type input in the website for words to get accepted by the website.
